#include "Game.hpp"

#include "engine/Fade.hpp"
#include "Control.hpp"
#include "Menu.hpp"
#include "Options.hpp"
#include "Fonts.hpp"
#include "Countdown.hpp"

#include <cmath>
#include <sstream>
#include <iomanip>
#include <utility>

Game::Game(std::shared_ptr<MouseTracker> mouse1, std::shared_ptr<MouseTracker> mouse2)
: player1(jngl::Vec2(-460, -50)), player2(jngl::Vec2(460, -50)), mouse1(std::move(mouse1)),
  mouse2(std::move(mouse2)) {
	player1.setOtherPlayer(&player2);
	player2.setOtherPlayer(&player1);
	static jngl::Font timerFont("DIGITALDREAMFAT.ttf", 30);
	timer.setFont(timerFont);
	timer.setText("00:00");
	timer.setCenter(0, -494);
}

void Game::step() {
	if (Options::handle().sound) {
	}
	if (jngl::keyPressed(jngl::key::Escape) || jngl::keyPressed(jngl::key::F10) ||
	    jngl::keyPressed('p') || mouse1->pause() || mouse2->pause()) {
		onBackEvent();
	}

	mouse1->step();
	mouse2->step();

	if (Options::handle().music && timeLeft == 123 * 60) {
		jngl::play("sfx/bgmusik.ogg");
	}

	if (timeLeft < 0) {
		endText.setAlign(jngl::Alignment::CENTER);
		endText.setFont(Fonts::handle().logo);
		if (player1.getPoints() > player2.getPoints()) {
			endText.setText("Spieler 1\ngewinnt mit\n" + std::to_string(player1.getPoints()) +
			                "\nPunkten");
		} else if (player1.getPoints() < player2.getPoints()) {
			endText.setText("Spieler 2\ngewinnt mit\n" + std::to_string(player2.getPoints()) +
			                "\nPunkten");
		} else {
			endText.setText("Gleichstand\nmit\n" + std::to_string(player2.getPoints()) +
			                "\nPunkten");
		}
		endText.setCenter(0, 0);

		if (jngl::keyPressed(jngl::key::Return) || mouse1->pause() || mouse2->pause()) {
			jngl::setWork<Fade>(std::make_shared<Countdown>());
		}
		return;
	}

	const float trashScale1 = player1.step(*mouse1);
	const float trashScale2 = player2.step(*mouse2);
	trashScale = std::max(trashScale1, trashScale2);

	std::stringstream text;
	text << "0" + std::to_string(timeLeft / jngl::getStepsPerSecond() / 60) +
	            ((timeLeft / 30) % 2 == 0 ? ":" : " ")
	     << std::setw(2) << std::setfill('0') << timeLeft / jngl::getStepsPerSecond() % 60;
	timer.setText(text.str());
	--timeLeft;
}

void Game::draw() const {
	jngl::draw("bg", jngl::getScreenSize() / -2);

	trash.draw(jngl::modelview().translate(jngl::Vec2(0, 390)).scale(trashScale));

	jngl::setSpriteColor(255, 255, 255);

	player1.draw();
	player2.draw();
	player1.drawTopLayer();
	player2.drawTopLayer();
	if (player1.isMirrored()) {
		jngl::pushMatrix();
		jngl::scale(1, -1);
	}
	if (timeLeft < GAME_DURATION) {
		mouse1->draw();
	}
	if (player1.isMirrored()) {
		jngl::popMatrix();
	}
	if (player2.isMirrored()) {
		jngl::pushMatrix();
		jngl::scale(1, -1);
	}
	if (timeLeft < GAME_DURATION) {
		mouse2->draw();
	}
	if (player2.isMirrored()) {
		jngl::popMatrix();
	}

	jngl::setFontColor(timeLeft <= 30 * 60 ? 0xee2222_rgb : 0x111111_rgb);
	timer.draw();
	if (timeLeft < 0) {
		jngl::setColor(255, 255, 255, 160);
		jngl::drawRect({ -3000, -3000 }, { 6000, 6000 });
		jngl::setFontColor(0x000000_rgb);
		endText.draw();
	}
}

bool Game::hasEnded() const {
	return false;
}

void Game::onBackEvent() {
	jngl::setWork<Menu>(shared_from_this());
}
