#include "Loading.hpp"

#include "Menu.hpp"
#include "Options.hpp"
#include "engine/Fade.hpp"
#include "Countdown.hpp"

#include <datafiles.hpp>
#include <jngl.hpp>
#include <sstream>
#include <thread>
#include <vector>

Loading::Loading() {
	std::istringstream filesString(SFX_FILES);
	std::string file;
	while (std::getline(filesString, file, ';')) {
		const std::string extension = ".ogg";
		if (file.substr(file.size() - extension.size()) == extension) {
			loadingFunctions.emplace_back([file]() {
				return jngl::load(file);
			});
		}
	}
}

void Loading::step() {
}

void Loading::draw() const {
	// float percentage = 100;
	if (currentIndex != loadingFunctions.size()) {
		// percentage = float(currentIndex) / loadingFunctions.size() * 100;
		std::vector<jngl::Finally> loaders;
		for (size_t i = 0; i < std::thread::hardware_concurrency(); ++i) {
			loaders.emplace_back(loadingFunctions[currentIndex]());
			++currentIndex;
			if (currentIndex == loadingFunctions.size()) {
				break;
			}
		}
	} else {
		finished = true;
	}
	jngl::setFontColor(0xffffff_rgb);
	jngl::setFontSize(30);
	if (finished) {
		jngl::setWork<Fade>(std::make_shared<Countdown>());
		// jngl::setWork<ScoreScreen>(Stats{.level = 1}, false, nullptr);
	} else {
		// jngl::print("loading ... ", 400, 400);
	}
}
