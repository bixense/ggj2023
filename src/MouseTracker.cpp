#include "MouseTracker.hpp"

#include <algorithm>

class KeyboardController1 : public jngl::Controller {
	bool down(jngl::controller::Button button) const override {
		if (button == jngl::controller::A) {
			return jngl::keyDown(jngl::key::Tab);
		}
		if (button == jngl::controller::Start) {
			return jngl::keyDown(jngl::key::Escape);
		}
		return false;
	}
	float stateImpl(jngl::controller::Button button) const override {
		if (button == jngl::controller::LeftStickX) {
			return (jngl::keyDown('a') ? -1 : 0) + (jngl::keyDown('d') ? 1 : 0);
		}
		if (button == jngl::controller::LeftStickY) {
			return (jngl::keyDown('w') ? 1 : 0) + (jngl::keyDown('s') ? -1 : 0);
		}
		return 0;
	}
};

class KeyboardController2 : public jngl::Controller {
	bool down(jngl::controller::Button button) const override {
		if (button == jngl::controller::A) {
			return jngl::keyDown(jngl::key::Space);
		}
		if (button == jngl::controller::Start) {
			return jngl::keyDown(jngl::key::Escape);
		}
		return false;
	}
	float stateImpl(jngl::controller::Button button) const override {
		if (button == jngl::controller::LeftStickX) {
			return (jngl::keyDown(jngl::key::Left) ? -1 : 0) +
			       (jngl::keyDown(jngl::key::Right) ? 1 : 0);
		}
		if (button == jngl::controller::LeftStickY) {
			return (jngl::keyDown(jngl::key::Up) ? 1 : 0) +
			       (jngl::keyDown(jngl::key::Down) ? -1 : 0);
		}
		return 0;
	}
};

MouseTracker::MouseTracker(int playerNr)
: controllerPos(playerNr == 1 ? jngl::Vec2(-600, 100) : jngl::Vec2(600, 100)), playerNr(playerNr) {
	auto update = [this]() {
		auto gamepads = jngl::getConnectedControllers();
		controllers.clear();
		for (size_t i = 0; i < gamepads.size(); ++i) {
			if (this->playerNr == 2) {
				if (i == 0) {
					controllers.emplace_back(gamepads[i]);
				}
			} else {
				if (i == 1) {
					controllers.emplace_back(gamepads[i]);
				}
			}
		}
		controllers.emplace_back(
		    this->playerNr == 1
		        ? std::shared_ptr<jngl::Controller>(std::make_shared<KeyboardController1>())
		        : std::shared_ptr<jngl::Controller>(std::make_shared<KeyboardController2>()));
	};
	jngl::onControllerChanged(update);
	update();
}

void MouseTracker::step() {
	for (const auto& controller : controllers) {
		jngl::Vec2 leftStick(controller->state(jngl::controller::LeftStickX),
		                     -controller->state(jngl::controller::LeftStickY));
		if (!controllerInUse &&
		    (boost::qvm::mag_sqr(leftStick) > 0.2 || controller->pressed(jngl::controller::A))) {
			controllerInUse = true;
			updateMouseVisible();
		}
		controllerPos += leftStick * 9;
	}
	controllerPos.x = std::clamp(controllerPos.x, playerNr == 2 ? 0 : -jngl::getScreenSize().x / 2.,
	                             playerNr == 1 ? 0 : jngl::getScreenSize().x / 2.);
	controllerPos.y =
	    std::clamp(controllerPos.y, -jngl::getScreenSize().y / 2., jngl::getScreenSize().y / 2.);
	if (controllerInUse && playerNr == 1 && jngl::mouseDown()) {
		controllerInUse = false;
		updateMouseVisible();
	}
}

void MouseTracker::draw() const {
	const double LINE_WIDTH = 6;
	const double LINE_LENGTH = 34;
	const double CENTER_GAP = 14;
	if (controllerInUse && visible) {
		jngl::setColor(0xffffff_rgb, 255);
		// oben
		jngl::drawRect(controllerPos + jngl::Vec2(-LINE_WIDTH / 2., -LINE_LENGTH - CENTER_GAP / 2.),
		               jngl::Vec2(LINE_WIDTH, LINE_LENGTH));
		jngl::setColor(0x000000_rgb, 255);
		jngl::drawRect(controllerPos +
		                   jngl::Vec2(-LINE_WIDTH / 2 / 2., -LINE_LENGTH - CENTER_GAP / 2.),
		               jngl::Vec2(LINE_WIDTH / 2, LINE_LENGTH));
		// rechts
		jngl::setColor(0xffffff_rgb, 255);
		jngl::drawRect(controllerPos + jngl::Vec2(CENTER_GAP / 2., -LINE_WIDTH / 2.),
		               jngl::Vec2(LINE_LENGTH, LINE_WIDTH));
		jngl::setColor(0x000000_rgb, 255);
		jngl::drawRect(controllerPos + jngl::Vec2(CENTER_GAP / 2., -LINE_WIDTH / 2. / 2.),
		               jngl::Vec2(LINE_LENGTH, LINE_WIDTH / 2.));
		// unten
		jngl::setColor(0xffffff_rgb, 255);
		jngl::drawRect(controllerPos + jngl::Vec2(-LINE_WIDTH / 2., CENTER_GAP / 2.),
		               jngl::Vec2(LINE_WIDTH, LINE_LENGTH));
		jngl::setColor(0x000000_rgb, 255);
		jngl::drawRect(controllerPos + jngl::Vec2(-LINE_WIDTH / 2. / 2., CENTER_GAP / 2.),
		               jngl::Vec2(LINE_WIDTH / 2., LINE_LENGTH));
		// links
		jngl::setColor(0xffffff_rgb, 255);
		jngl::drawRect(controllerPos + jngl::Vec2(-CENTER_GAP / 2. - LINE_LENGTH, -LINE_WIDTH / 2.),
		               jngl::Vec2(LINE_LENGTH, LINE_WIDTH));
		jngl::setColor(0x000000_rgb, 255);
		jngl::drawRect(controllerPos +
		                   jngl::Vec2(-CENTER_GAP / 2. - LINE_LENGTH, -LINE_WIDTH / 2. / 2.),
		               jngl::Vec2(LINE_LENGTH, LINE_WIDTH / 2.));
	}
}

optional<jngl::Vec2> MouseTracker::cursorPos() const {
	if (controllerInUse) {
		return controllerPos;
	}
	return jngl::getCursorPos();
}

jngl::Vec2 MouseTracker::pos() const {
	if (controllerInUse) {
		return controllerPos;
	}
	return jngl::getMousePos();
}

bool MouseTracker::down() const {
	if (controllerInUse) {
		return std::any_of(controllers.begin(), controllers.end(), [](const auto& controller) {
			return controller->down(jngl::controller::A);
		});
	}
	return jngl::mouseDown();
}

bool MouseTracker::pressed() const {
	if (controllerInUse) {
		return std::any_of(controllers.begin(), controllers.end(), [](const auto& controller) {
			return controller->pressed(jngl::controller::A);
		});
	}
	return jngl::mousePressed();
}

void MouseTracker::setVisible(bool visible) {
	this->visible = visible;
	updateMouseVisible();
}

bool MouseTracker::pause() const {
	if (controllerInUse) {
		return std::any_of(controllers.begin(), controllers.end(), [](const auto& controller) {
			return controller->pressed(jngl::controller::Start);
		});
	}
	return jngl::keyPressed(jngl::key::Escape) || jngl::keyPressed('p');
}

bool MouseTracker::back() const {
	if (controllerInUse) {
		return std::any_of(controllers.begin(), controllers.end(), [](const auto& controller) {
			return controller->pressed(jngl::controller::B);
		});
	}
	return jngl::keyPressed(jngl::key::Escape);
}

void MouseTracker::updateMouseVisible() {
	// jngl::setMouseVisible(!controllerInUse && visible);
}
