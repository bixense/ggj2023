#pragma once

#include "Pot.hpp"

class Card;
class MouseTracker;

class Player {
public:
	explicit Player(jngl::Vec2 potPosition);
	float step(MouseTracker&);
	void draw() const;
	void drawTopLayer() const;
	void setOtherPlayer(Player*);
	bool isMirrored() const;
	int getPoints() const;

private:
	void restoreHandPositions();
	void resetHand();
	std::shared_ptr<Card> randomCard();

	jngl::Vec2 potPosition;
	Pot pot;

	std::vector<std::shared_ptr<Card>> deck;
	std::vector<std::shared_ptr<Card>> hand;
	std::vector<std::shared_ptr<Card>> ablage;
	std::weak_ptr<Card> cardOnTop; //< nur für die Zeichenreihenfolge relevant
	std::shared_ptr<Card> selectedCard;
	jngl::Vec2 mouseDiff;
	std::shared_ptr<Card> lastDroppedCard;

	double direction;
	jngl::Sprite cardback{"cardback"};
	jngl::Vec2 cardbackCenter{ -800 * direction, 500 };
	jngl::Vec2 cardbackDrawCenter{ -800 * direction, 600 };
	float cardbackMouseoverFactor = 0.f; //< geht von 0 bis 1 (mouseover)
	jngl::Text penaltyText;

	int points = 0;
	float drawPoints = 0;
	jngl::Text pointsText;

	int blinded = 0;
	int shrink = 0;
	int increase = 0;
	int mirrored = 0;

	Player* otherPlayer = nullptr;
	jngl::Vec2 notepadPos;

	float trashScale = 1.1f;

	float potScale = 1.f;

	int newCardCountdown = -1;

	jngl::Sprite scoreboard{"scoreboard"};
};
