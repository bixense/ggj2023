#include "Player.hpp"

#include "Card.hpp"
#include "cards.hpp"
#include "Fonts.hpp"
#include "MouseTracker.hpp"
#include "Options.hpp"

Player::Player(jngl::Vec2 potPosition)
: potPosition(potPosition), direction(this->potPosition.x > 0 ? -1 : 1),
  notepadPos(potPosition + jngl::Vec2(-320 * direction, -320)) {
	resetHand();
	penaltyText.setText(std::to_string(pot.getPenalty()) + " kcal");
	penaltyText.setCenter(cardbackDrawCenter + jngl::Vec2(0, -90));
	scoreboard.setCenter(-250 * direction, -499);
}

float Player::step(MouseTracker& mouse) {
	--blinded;
	--shrink;
	--increase;
	--mirrored;
	float potScaleTarget = 1.f;
	if (increase > 0) {
		potScaleTarget += 0.5;
	}
	if (shrink > 0) {
		potScaleTarget -= 0.5;
	}
	potScale += 0.1f * (potScaleTarget - potScale);

	if (const auto card = cardOnTop.lock()) {
		if (!card->isAnimating()) {
			cardOnTop.reset();
		}
	}
	bool potMouseOver = false;
	bool trashMouseOver = false;
	if (selectedCard) {
		selectedCard->setCenter(mouse.pos() - mouseDiff);
		potMouseOver =
		    boost::qvm::mag(selectedCard->getCenter() - potPosition) < (potScale * pot.getRadius());
		trashMouseOver = boost::qvm::mag(selectedCard->getCenter() - jngl::Vec2(0, 540)) < 200;
		if (!mouse.down()) {
			if (potMouseOver) {
				pot.addCard(selectedCard, potScaleTarget > 1 ? 2 : potScaleTarget);
				if (Options::handle().sound) {
					jngl::play("sfx/insert.ogg");
				}
				switch (selectedCard->getSpecial()) {
				case Special::DRAW_CARD:
					hand.emplace_back(randomCard());
					hand.back()->setCenter(cardbackCenter);
					restoreHandPositions();
					break;
				case Special::FILL_HAND:
					while (hand.size() < 5 /* eine wird gleich entfernt */) {
						hand.emplace_back(randomCard());
						hand.back()->setCenter(cardbackCenter);
					}
					restoreHandPositions();
					break;
				case Special::POT_INCREASE:
					increase = static_cast<int>(jngl::getStepsPerSecond()) * 10;
				default:
					break;
				}
				switch (selectedCard->getAttack()) {
				case Attack::NONE:
					break;
				case Attack::SALT_OVERLOAD:
					otherPlayer->pot.adjustSalt(40);
					break;
				case Attack::HEAT_OVERLOAD:
					otherPlayer->pot.adjustHeat(90);
					break;
				case Attack::BLIND:
					otherPlayer->blinded = static_cast<int>(jngl::getStepsPerSecond()) * 4;
					break;
				case Attack::BLIND_SELF:
					blinded = static_cast<int>(jngl::getStepsPerSecond()) * 4;
					break;
				case Attack::POT_SHRINK:
					otherPlayer->shrink = static_cast<int>(jngl::getStepsPerSecond()) * 10;
					break;
				case Attack::MIRROR:
					otherPlayer->mirrored = 9 * 60;
					break;
				}
			}
			if (trashMouseOver || potMouseOver) {
				if (trashMouseOver && Options::handle().sound) {
					jngl::play("sfx/recycle.ogg");
				}
				hand.erase(std::find_if(hand.begin(), hand.end(), [this](const auto& c) {
					return c.get() == selectedCard.get();
				}));
				lastDroppedCard = selectedCard;
				ablage.push_back(lastDroppedCard);
				lastDroppedCard->startRemove();
			}
			cardOnTop = selectedCard; // für die Animation zurück soll sie über den anderen
			                          // gezeichnet werden
			selectedCard.reset();
			restoreHandPositions();
			if (hand.empty()) {
				newCardCountdown = 3 * 60;
			}
		}
	}
	bool notepadMouseOver =
	    pot.sellable() && boost::qvm::mag_sqr(mouse.pos() - notepadPos) < 190 * 190;
	pot.step(potMouseOver, notepadMouseOver);
	trashScale += 0.2f * ((trashMouseOver ? 1.4f : 1.f) - trashScale);
	if (notepadMouseOver && mouse.pressed()) {
		points += pot.sell();
		resetHand();
	}
	drawPoints += 0.2f * (static_cast<float>(points) - drawPoints);
	if (lastDroppedCard) {
		lastDroppedCard->step();
	}
	bool anyCardMouseOver = false;
	for (auto& card : hand) {
		card->step();
		if ((!selectedCard || card == selectedCard) && card->checkMouseOver(mouse.pos(), true)) {
			anyCardMouseOver = true;
			if (mouse.pressed()) {
				selectedCard = card;
				mouseDiff = mouse.pos() - card->getCenter();
			}
		}
	}
	const bool mayDrawCard = hand.size() < 4;
	const bool cardbackMouseover =
	    mayDrawCard && !anyCardMouseOver &&
	    ((cardbackCenter - cardback.getSize() / 2.).x <= mouse.pos().x &&
	     mouse.pos().x < (cardbackCenter - cardback.getSize() / 2.).x + cardback.getWidth() &&
	     (cardbackCenter - cardback.getSize() / 2.).y <= mouse.pos().y &&
	     mouse.pos().y < (cardbackCenter - cardback.getSize() / 2.).y + cardback.getHeight());
	cardbackMouseoverFactor += 0.1f * ((cardbackMouseover ? 1.0f : 0.f) - cardbackMouseoverFactor);
	if (cardbackMouseover && mouse.pressed()) {
		cardbackMouseoverFactor = 0;
		if (pot.drawCardPenalty()) {
			jngl::play("sfx/draw.ogg");
			hand.emplace_back(randomCard());
			hand.back()->setCenter(cardbackCenter);
			restoreHandPositions();
		}
	}
	cardbackDrawCenter +=
	    0.2 * ((mayDrawCard ? cardbackCenter : (cardbackCenter + jngl::Vec2(0, 150))) -
	           cardbackDrawCenter);

	pointsText.setFont(points > otherPlayer->points ? Fonts::handle().notepadBig
	                                                : Fonts::handle().notepad);
	pointsText.setText(std::to_string(std::lround(drawPoints)));
	pointsText.setCenter(-250 * direction, -499);

	--newCardCountdown;
	if (newCardCountdown == 0 && hand.empty()) {
		hand.emplace_back(randomCard());
		hand.back()->setCenter(cardbackCenter);
		restoreHandPositions();
		jngl::play("sfx/draw.ogg");
	}
	return trashScale;
}

void Player::draw() const {
	jngl::pushMatrix();
	if (mirrored > 0) {
		jngl::scale(1, -1);
	}
	jngl::pushMatrix();
	jngl::translate(potPosition);
	jngl::scale(potScale);
	pot.draw();
	jngl::popMatrix();
	jngl::pushMatrix();
	jngl::translate(notepadPos);
	pot.drawStats(blinded > 0);
	jngl::popMatrix();
	for (int i = 0; i < 8; ++i) {
		cardback.draw(jngl::modelview()
		                  .translate(cardbackDrawCenter +
		                             jngl::Vec2(-100 * direction +
		                                            i * direction * (14 - cardbackMouseoverFactor),
		                                        i * -2))
		                  .scale(i == 7 ? (1.f + cardbackMouseoverFactor * 0.1f) : 1));
	}
	jngl::setColor(255, 255, 255, 150 * cardbackMouseoverFactor);
	jngl::drawRect(penaltyText.getCenter() - jngl::Vec2(90, 40), jngl::Vec2(180, 70));
	jngl::setFontColor(200, 40, 40, 255 * cardbackMouseoverFactor);
	penaltyText.draw();

	const auto cardOnTopShared = cardOnTop.lock();
	for (auto& card : hand) {
		if (card != cardOnTopShared && !card->isMouseOver()) {
			card->draw();
		}
	}
	for (auto& card : hand) {
		if (card != cardOnTopShared && card->isMouseOver()) {
			card->draw();
		}
	}
	jngl::popMatrix();
	if (lastDroppedCard) {
		lastDroppedCard->draw();
	}

	scoreboard.draw();
	jngl::setFontColor(0x111111_rgb);
	pointsText.draw();
}

void Player::drawTopLayer() const {
	jngl::pushMatrix();
	if (mirrored > 0) {
		jngl::scale(1, -1);
	}
	if (const auto cardOnTopShared = cardOnTop.lock()) {
		cardOnTopShared->draw();
	}
	if (selectedCard) {
		selectedCard->draw();
	}
	if (hand.empty() && newCardCountdown > 0) {
		jngl::setFontColor(0x222222_rgb);
		jngl::print("Neue Karte in " + std::to_string(newCardCountdown / 60 + 1) + " s",
		            potPosition + jngl::Vec2(-120, 300));
	}
	jngl::popMatrix();
}

void Player::setOtherPlayer(Player* other) {
	otherPlayer = other;
}

bool Player::isMirrored() const {
	return mirrored > 0;
}

int Player::getPoints() const {
	return points;
}

void Player::restoreHandPositions() {
	int i = 0;
	const double cardWidth = 205;
	for (auto& card : hand) {
		card->moveTo(
		    potPosition +
		    jngl::Vec2{ -(cardWidth * static_cast<double>(hand.size() - 1)) / 2. + i * cardWidth,
		                370. });
		++i;
	}
}

void Player::resetHand() {
	hand.clear();
	for (int i = hand.size(); i < 3; ++i) {
		hand.emplace_back(randomCard());
		hand.back()->setCenter(cardbackCenter);
	}
	restoreHandPositions();
	jngl::play("sfx/draw.ogg");
}

std::shared_ptr<Card> Player::randomCard() {
	if (deck.empty()) {
		deck = createNewDeck();
	}

	auto card = deck.back();
	deck.pop_back();
	return card;
};
