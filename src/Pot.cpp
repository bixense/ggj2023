#include "Pot.hpp"

#include "Card.hpp"
#include "Fonts.hpp"
#include "Options.hpp"

Pot::Pot() {
	kcalText.setFont(Fonts::handle().notepad);
	celciusText.setFont(Fonts::handle().notepad);
	saltDescription.setFont(Fonts::handle().notepadSmall);
	saltText.setFont(Fonts::handle().notepad);
}

void Pot::step(bool mouseOver, bool notepadMouseOver) {
	kcalDraw += 0.2f * (kcal - kcalDraw);
	celciusDraw += 0.004f * (static_cast<float>(celcius) - celciusDraw);
	saltDraw += 0.1f * (static_cast<float>(salt) - saltDraw);

	kcalText.setText(std::to_string(std::lround(kcalDraw)) + " kcal");
	kcalText.setPos(86 - kcalText.getWidth(), -108);

	celciusText.setText(std::to_string(std::lround(celciusDraw)) + " °C");
	celciusText.setPos(88 - celciusText.getWidth(), -45);

	if (std::lround(salt) < MIN_ACCEPTABLE_SALT) {
		saltDescription.setText("zu wenig Salz:");
	} else if (std::lround(salt) >= MAX_ACCEPTABLE_SALT) {
		saltDescription.setText("zu viel Salz:");
	} else {
		saltDescription.setText("Salz:");
	}
	saltText.setText(std::to_string(std::lround(saltDraw)) + " mg");
	saltText.setPos(91 - saltText.getWidth(), 52);

	scale += 0.2f * ((mouseOver ? 1.2f : 1.f) - scale);
	notepadScale += 0.2f * ((notepadMouseOver ? 1.2f : 1.f) - notepadScale);

	saltGood = std::lround(salt) >= MIN_ACCEPTABLE_SALT && std::lround(salt) < MAX_ACCEPTABLE_SALT;
	celciusGood = std::lround(celciusDraw) > MIN_ACCEPTABLE_HEAT &&
	              std::lround(celciusDraw) < MAX_ACCEPTABLE_HEAT;

	if (celciusDraw >= MAX_ACCEPTABLE_HEAT) {
		kcal -= 0.007f;
	}
	if (celciusDraw >= 100) {
		kcal -= 0.009f;
	}
	if (celciusDraw >= 150) {
		kcal -= 0.01f;
	}
	if (celciusDraw >= 200) {
		kcal -= 0.035f;
	}
	if (celciusDraw >= 300) {
		kcal -= 0.09f;
	}
	if (kcal < 0) {
		kcal = 0;
	}

	container.step();
}

void Pot::draw() const {
	if (std::lround(celciusDraw) >= MAX_ACCEPTABLE_HEAT) {
		jngl::setSpriteColor(255, 100, 100);
		sprite.draw(
		    jngl::modelview()
		        .translate(jngl::Vec2(sin(jngl::getTime() * 17) * 3, sin(jngl::getTime() * 23) * 3))
		        .scale(scale));
	} else if (std::lround(celciusDraw) <= MIN_ACCEPTABLE_HEAT) {
		jngl::setSpriteColor(100, 100, 255);
		sprite.draw(jngl::modelview().scale(scale));
	} else {
		sprite.draw(jngl::modelview().scale(scale));
	}
	jngl::setSpriteColor(255, 255, 255);

	container.draw();
}

void Pot::drawStats(bool blinded) const {
	jngl::pushMatrix();
	jngl::scale(notepadScale);
	if (blinded) {
		LegalPadBlind.draw();
	} else {
		legalPad.draw();
		jngl::setFontColor(0x333333_rgb);
		kcalText.draw();
		jngl::setFontColor(celciusGood ? 0x336633_rgb : 0xbb3333_rgb);
		celciusText.draw();
		jngl::setFontColor(saltGood ? 0x336633_rgb : 0xbb3333_rgb);
		saltText.draw();
		jngl::pushMatrix();
		jngl::translate(-110, 26);
		jngl::rotate(-2);
		saltDescription.draw();
		jngl::popMatrix();
	}
	jngl::popMatrix();
}

void Pot::addCard(std::shared_ptr<Card> card, float multiplier) {
	kcal += card->getKcal() * multiplier;
	if (multiplier < 0.9 || multiplier > 1.1) {
		const auto label = container.addWidget<jngl::Label>(multiplier < 1 ? "0,5x" : "2x",
		                                                    Fonts::handle().headline, 0x337eaa_rgb,
		                                                    jngl::Vec2(0, 0));
		label->addEffect<jngl::Zoom>([](float t) { return jngl::easing::elastic(t); });
		label->addEffect<jngl::Move>(jngl::Vec2(0, -500), [](float t) { return 1 - t; });
		label->addEffect<jngl::Executor>([label](float t) {
			label->setAlpha(1 - t / 2.f);
			return t > 2 ? jngl::Effect::Action::REMOVE_WIDGET : jngl::Effect::Action::NONE;
		});
	}
	if (kcal < 0) {
		kcal = 0;
	}
	celcius += card->getCelcius();
	adjustSalt(card->getSalt());
	switch (card->getSpecial()) {
	case Special::NONE:
		break;
	case Special::RESET_SALT:
		salt = 0;
		break;
	case Special::HALVE_SALT:
		salt /= 2;
		break;
	case Special::RESET_HEAT:
		celcius = 20;
		break;
	case Special::RESET_ALL:
		celcius = 20;
		salt = 0;
		kcal = 0;
		break;
	case Special::HALVE_HEAT:
		celcius /= 2;
		break;
	case Special::DOUBLE_KCAL:
		kcal *= 2;
		break;
	case Special::DRAW_CARD:
	case Special::FILL_HAND:
	case Special::POT_INCREASE:
		// macht der Player
		break;
	}
	cards.emplace_back(std::move(card));
}

bool Pot::drawCardPenalty() {
	if (kcal < -getPenalty()) {
		return false;
	}
	kcal += getPenalty();
	if (kcal < 0) {
		kcal = 0;
	}
	return true;
}

void Pot::adjustSalt(int difference) {
	salt += difference;
	if (salt < 0) {
		salt = 0;
	}
}

void Pot::adjustHeat(int difference) {
	celcius += difference;
}

bool Pot::sellable() const {
	return saltGood && celciusGood;
}

int Pot::sell() {
	if (Options::handle().sound) {
		jngl::play("sfx/checkout.ogg");
	}
	notepadScale = 1;
	int points = kcal;
	kcal = 0;
	salt = 0;
	cards.clear();
	return points;
}

int Pot::getRadius() {
	return 190;
}

int Pot::getPenalty() const {
	return -50;
}
