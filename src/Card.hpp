#pragma once

#include <memory>
#include <jngl.hpp>

enum class Special {
	NONE,
	RESET_SALT,
	HALVE_SALT,
	RESET_HEAT,
	HALVE_HEAT,
	DRAW_CARD,
	FILL_HAND,
	DOUBLE_KCAL,
	POT_INCREASE,
	RESET_ALL,
};

enum class Attack {
	NONE,
	SALT_OVERLOAD,
	HEAT_OVERLOAD,
	BLIND,
	BLIND_SELF,
	POT_SHRINK,
	MIRROR,
};

class Card {
public:
	Card(std::string name, int kcal, int celcius, int salt, Special, Attack);
	void step();
	void draw() const;
	void moveTo(jngl::Vec2 center);
	jngl::Vec2 getCenter() const;
	void setCenter(jngl::Vec2 center);
	bool checkMouseOver(jngl::Vec2 mousePos, bool sensitive);
	bool isMouseOver() const;
	bool isAnimating() const;

	/// startet Animation zum verschwinden
	void startRemove();

	int getKcal() const;
	int getCelcius() const;
	int getSalt() const;
	Special getSpecial() const;
	Attack getAttack() const;

private:
	jngl::Sprite sprite;
	jngl::Sprite picture;
	std::string name;
	int kcal;
	int celcius;
	int salt;
	jngl::Text headline;
	jngl::Text kcalText;
	jngl::Text celciusText;
	jngl::Text saltText;
	jngl::Vec2 targetCenter;
	jngl::Vec2 center;
	jngl::Vec2 size;
	bool mouseOver = false;
	bool remove = false;
	float scale = 1.f;
	static std::shared_ptr<jngl::Font> font;
	static std::shared_ptr<jngl::Font> fontSmall;
	Special special;
	jngl::Text specialText;
	Attack attack;
	jngl::Text attackText;
};
