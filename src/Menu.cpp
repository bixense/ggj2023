#include "Menu.hpp"

#include "Countdown.hpp"
#include "engine/Fade.hpp"
#include "Game.hpp"
#include "Options.hpp"

#include <fstream>

void replaceAll(std::string& subject, const std::string& search, const std::string& replace) {
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != std::string::npos) {
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
}

std::stringstream loadAndReplace(int width, int height) {
	std::ifstream source("blur.frag");
	std::stringstream buffer;
	buffer.exceptions(std::ios_base::failbit);
	buffer << source.rdbuf();
	std::string tmp = buffer.str();
	replaceAll(tmp, "FBO_WIDTH", std::to_string(width) + ".f");
	replaceAll(tmp, "FBO_HEIGHT", std::to_string(height) + ".f");
	return std::stringstream(tmp);
}

std::stringstream loadAndReplaceGLES20(int width, int height) {
	std::ifstream source("blur.gles20.frag");
	std::stringstream buffer;
	buffer.exceptions(std::ios_base::failbit);
	buffer << source.rdbuf();
	std::string tmp = buffer.str();
	replaceAll(tmp, "FBO_WIDTH", std::to_string(width) + ".0");
	replaceAll(tmp, "FBO_HEIGHT", std::to_string(height) + ".0");
	return std::stringstream(tmp);
}

Menu::Menu(const std::shared_ptr<jngl::Work>& game)
: game(std::dynamic_pointer_cast<Game>(game)),
  fbo(jngl::getWindowSize()[0] / REDUCE_RESOLUTION, jngl::getWindowSize()[1] / REDUCE_RESOLUTION),
  fbo2(jngl::getWindowSize()[0] / REDUCE_RESOLUTION, jngl::getWindowSize()[1] / REDUCE_RESOLUTION),
  fragment(
      loadAndReplace(static_cast<int>(std::lround(jngl::getWindowWidth() / REDUCE_RESOLUTION)),
                     static_cast<int>(std::lround(jngl::getWindowHeight() / REDUCE_RESOLUTION))),
      jngl::Shader::Type::FRAGMENT,
      loadAndReplaceGLES20(
          static_cast<int>(std::lround(jngl::getWindowWidth() / REDUCE_RESOLUTION)),
          static_cast<int>(std::lround(jngl::getWindowHeight() / REDUCE_RESOLUTION)))),
  blur(jngl::Sprite::vertexShader(), fragment),
  music(
      std::string("Music: ") + (Options::handle().music ? "On" : "Off"), jngl::Vec2(0, 0),
      [this]() {
	Options::handle().music = !Options::handle().music;
	if (Options::handle().music) {
		jngl::loop("sfx/bgmusik.ogg");
	} else {
		jngl::stop("sfx/bgmusik.ogg");
	}
	music.setLabel(std::string("Music: ") + (Options::handle().music ? "On" : "Off"));
      },
      true),
sound(
    std::string("Sound: ") + (Options::handle().sound ? "On" : "Off"), jngl::Vec2(0, 90),
    [this]() {
	Options::handle().sound = !Options::handle().sound;
	sound.setLabel(std::string("Sound: ") + (Options::handle().sound ? "On" : "Off"));
    },
    true) {
	if (!this->game->hasEnded()) {
		container.addWidget<Button>(
		    "Resume", jngl::Vec2(0, -180), [this]() { onQuitEvent(); }, true);
	}
	container.addWidget<Button>(
	    "Restart", jngl::Vec2(0, -90),
	    [this]() { jngl::setWork<Fade>(std::make_shared<Countdown>()); }, true);
	if (jngl::canQuit()) {
		container.addWidget<Button>(
		    "Quit", jngl::Vec2(0, 180), [this]() { jngl::quit(); }, true);
	}
}

Menu::~Menu() {
	Options::handle().save();
}

void Menu::step() {
	container.step();
	music.step();
	sound.step();
	if (jngl::keyPressed(jngl::key::Escape)) {
		onQuitEvent();
	}
}

void Menu::draw() const {
	{
		auto context = fbo.use();
		jngl::scale(1. / REDUCE_RESOLUTION);
		game->draw();
	}
	{
		auto context = fbo2.use();
		fbo.draw(jngl::modelview(), &blur);
	}
	fbo2.draw(jngl::modelview().scale(REDUCE_RESOLUTION));
	container.draw();
	music.draw();
	sound.draw();
}

void Menu::onQuitEvent() {
	jngl::setWork(this->game);
}
