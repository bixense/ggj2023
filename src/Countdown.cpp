#include "Countdown.hpp"

#include "engine/Fade.hpp"
#include "Game.hpp"
#include "Options.hpp"
#include "Fonts.hpp"

#include <fstream>
#include <random>

void replaceAll(std::string& subject, const std::string& search, const std::string& replace);

std::stringstream loadAndReplace(int width, int height);
std::stringstream loadAndReplaceGLES20(int width, int height);

Countdown::Countdown()
: game(std::make_shared<Game>(mouse1, mouse2)),
  fbo(jngl::getWindowSize()[0] / REDUCE_RESOLUTION, jngl::getWindowSize()[1] / REDUCE_RESOLUTION),
  fbo2(jngl::getWindowSize()[0] / REDUCE_RESOLUTION, jngl::getWindowSize()[1] / REDUCE_RESOLUTION),
  fragment(
      loadAndReplace(static_cast<int>(std::lround(jngl::getWindowWidth() / REDUCE_RESOLUTION)),
                     static_cast<int>(std::lround(jngl::getWindowHeight() / REDUCE_RESOLUTION))),
      jngl::Shader::Type::FRAGMENT,
      loadAndReplaceGLES20(
          static_cast<int>(std::lround(jngl::getWindowWidth() / REDUCE_RESOLUTION)),
          static_cast<int>(std::lround(jngl::getWindowHeight() / REDUCE_RESOLUTION)))),
  blur(jngl::Sprite::vertexShader(), fragment),
  ready1(container.addWidget<jngl::Label>("Spieler 1 bereit?", Fonts::handle().normal, 0x222222_rgb,
                                          jngl::Vec2(-400, 0))),
  ready2(container.addWidget<jngl::Label>("Spieler 2 bereit?", Fonts::handle().normal, 0x222222_rgb,
                                          jngl::Vec2(400, 0))) {
}

Countdown::~Countdown() {
	Options::handle().save();
}

void Countdown::step() {
	if (ready1 == nullptr && ready2 == nullptr) {
		++framesPassed;
	}
	if (framesPassed == 0) {
		const auto label = container.addWidget<jngl::Label>("3", Fonts::handle().logo, 0x222222_rgb,
		                                                    jngl::Vec2(0, 0));
		label->addEffect<jngl::Zoom>([](float t) { return jngl::easing::elastic(t); });
		label->addEffect<jngl::Executor>([label](float t) {
			return t > 1 ? jngl::Effect::Action::REMOVE_WIDGET : jngl::Effect::Action::NONE;
		});
		if (Options::handle().sound) {
			static std::random_device rd;
			static std::mt19937 g(rd());
			if (g() % 100 < 99) {
				jngl::play("sfx/32eintopf.ogg");
			} else {
				jngl::play("sfx/321topf.ogg");
			}
		}
	}
	if (framesPassed == 60) {
		const auto label = container.addWidget<jngl::Label>("2", Fonts::handle().logo, 0x222222_rgb,
		                                                    jngl::Vec2(0, 0));
		label->addEffect<jngl::Zoom>([](float t) { return jngl::easing::elastic(t); });
		label->addEffect<jngl::Executor>([label](float t) {
			return t > 1 ? jngl::Effect::Action::REMOVE_WIDGET : jngl::Effect::Action::NONE;
		});
	}
	if (framesPassed == 120) {
		const auto label = container.addWidget<jngl::Label>("1topf!", Fonts::handle().logo,
		                                                    0x222222_rgb, jngl::Vec2(0, 0));
		label->addEffect<jngl::Zoom>([](float t) { return jngl::easing::elastic(t); });
		label->addEffect<jngl::Executor>([label](float t) {
			return t > 1 ? jngl::Effect::Action::REMOVE_WIDGET : jngl::Effect::Action::NONE;
		});
	}

	container.step();
	if (jngl::keyPressed(jngl::key::Escape) || framesPassed > 3 * 60) {
		onQuitEvent();
	}
	mouse1->step();
	mouse2->step();
	if (mouse1->pressed()) {
		container.removeWidget(ready1);
		ready1 = nullptr;
	}
	if (mouse2->pressed()) {
		container.removeWidget(ready2);
		ready2 = nullptr;
	}
}

void Countdown::draw() const {
	{
		auto context = fbo.use();
		jngl::scale(1. / REDUCE_RESOLUTION);
		game->draw();
	}
	{
		auto context = fbo2.use();
		fbo.draw(jngl::modelview(), &blur);
	}
	fbo2.draw(jngl::modelview().scale(REDUCE_RESOLUTION));
	container.draw();
}

void Countdown::onQuitEvent() {
	jngl::setWork(this->game);
}
