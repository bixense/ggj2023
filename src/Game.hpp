#pragma once

#include "Card.hpp"
#include "Player.hpp"
#include "MouseTracker.hpp"

class Game : public jngl::Work, public std::enable_shared_from_this<Game> {
public:
	Game(std::shared_ptr<MouseTracker> mouse1, std::shared_ptr<MouseTracker> mouse2);
	void step() override;
	void draw() const override;
	bool hasEnded() const;

private:
	void onBackEvent() override;

	Player player1;
	Player player2;
	std::shared_ptr<MouseTracker> mouse1;
	std::shared_ptr<MouseTracker> mouse2;

	jngl::Sprite trash{ "trash" };
	float trashScale = 1.1f;

	jngl::Text endText;
	jngl::Text timer;
	constexpr static int GAME_DURATION = 180 * 60;
	int timeLeft = GAME_DURATION;
};
