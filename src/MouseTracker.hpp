#pragma once

#include <jngl.hpp>

class MouseTracker : public jngl::Job {
public:
	MouseTracker(int playerNr);

	void step() override;
	void draw() const override;

	/// Gibt die Maus-Position zurück, NICHT touch
	optional<jngl::Vec2> cursorPos() const;

	jngl::Vec2 pos() const;

	bool down() const;
	bool pressed() const;
	void setVisible(bool);
	bool pause() const;
	bool back() const;

private:
	void updateMouseVisible();

	std::vector<std::shared_ptr<jngl::Controller>> controllers;
	jngl::Vec2 controllerPos;

	/// ob die Maus oder der virtuelle Cursor sichtbar sein soll
	bool visible = true;

	int playerNr;
	bool controllerInUse = playerNr != 1;
};
