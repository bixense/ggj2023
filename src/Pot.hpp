#pragma once

#include <jngl.hpp>
#include <memory>

class Card;

class Pot {
public:
	explicit Pot();
	void step(bool mouseOver, bool);
	void draw() const;
	void drawStats(bool blinded) const;

	void addCard(std::shared_ptr<Card> card, float multiplier);

	/// gibt true zurück wenn eine Karte gezogen werden kann
	bool drawCardPenalty();

	void adjustSalt(int);
	void adjustHeat(int);

	bool sellable() const;
	int sell();
	int getRadius();

	int getPenalty() const;

private:
	jngl::Sprite sprite{"pot"};
	jngl::Sprite legalPad{"LegalPad"};
	jngl::Sprite LegalPadBlind{"LegalPadBlind"};
	float scale = 0.f;
	float notepadScale = 0.5f;

	std::vector<std::shared_ptr<Card>> cards;
	float kcal = 0;
	float kcalDraw = 0;
	jngl::Text kcalText;
	int celcius = 90;
	float celciusDraw = 0;
	jngl::Text celciusText;
	bool celciusGood = false;

	constexpr static int MIN_ACCEPTABLE_SALT = 10;
	constexpr static int MAX_ACCEPTABLE_SALT = 50;
	constexpr static int MIN_ACCEPTABLE_HEAT = 30;
	constexpr static int MAX_ACCEPTABLE_HEAT = 90;
	int salt = 0;
	float saltDraw = 0;
	jngl::Text saltText;
	jngl::Text saltDescription;
	bool saltGood = false;

	jngl::Container container;
};
