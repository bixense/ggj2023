#include "cards.hpp"

#include "Card.hpp"

#include <random>
#include <vector>

std::vector<std::shared_ptr<Card>> createCommonCards() {
	// clang-format off
	return {
		std::make_shared<Card>("Karotte",         25, -30,  4, Special::NONE,         Attack::NONE),
		std::make_shared<Card>("Knollensellerie", 90, -30,  3, Special::NONE,         Attack::NONE),
		std::make_shared<Card>("Pastinake",       20,   0,  5, Special::FILL_HAND,    Attack::NONE),
	};
	// clang-format on
}

std::vector<std::shared_ptr<Card>> createNormalCards() {
	// clang-format off
	return {
		std::make_shared<Card>("Kartoffel",      200, -20,  0, Special::RESET_SALT,   Attack::NONE),
		std::make_shared<Card>("Süßkartoffel",    80,  50,  2, Special::POT_INCREASE, Attack::NONE),
		std::make_shared<Card>("Rote Bete",       30,  10,  0, Special::HALVE_SALT,   Attack::NONE),
		std::make_shared<Card>("Nachtkerze",      80,  80,  2, Special::NONE,         Attack::NONE),
		std::make_shared<Card>("Haferwurzel",     20,   0,  4, Special::HALVE_HEAT,   Attack::NONE),
		std::make_shared<Card>("Rettich",         40,   0,  3, Special::DRAW_CARD,    Attack::NONE),
		std::make_shared<Card>("Yambohne",        20,   0, -8, Special::NONE,         Attack::NONE),
		std::make_shared<Card>("Schwarzwurzel",  -50,   0,  5, Special::RESET_HEAT,   Attack::NONE),
		std::make_shared<Card>("Zuckerwurzel",    50, -10,  5, Special::NONE,         Attack::POT_SHRINK),
		std::make_shared<Card>("Mohnblume",       40,  40, 40, Special::NONE,         Attack::BLIND_SELF),
	};
	// clang-format on
}

std::vector<std::shared_ptr<Card>> createSpecialCards() {
	// clang-format off
	return {
		std::make_shared<Card>("Glutamat",         0,   0, 10, Special::DOUBLE_KCAL,  Attack::NONE),
		std::make_shared<Card>("Radieschen",     150,  60, 30, Special::NONE,      Attack::BLIND),
		std::make_shared<Card>("Hanky",            0,   0,  0, Special::RESET_ALL, Attack::NONE),
		std::make_shared<Card>("Mr. Karotti",   -120,  40,  0, Special::NONE,      Attack::SALT_OVERLOAD),
		std::make_shared<Card>("Mrs. Möhre",      30, -30,  0, Special::NONE,      Attack::HEAT_OVERLOAD),
		std::make_shared<Card>("Zauberkraut",    100,   0,  6, Special::NONE,      Attack::MIRROR),
		std::make_shared<Card>("Niete",            0,   0,  0, Special::DRAW_CARD, Attack::NONE),
	};
	// clang-format on
}

std::vector<std::shared_ptr<Card>> createNewDeck() {
	static std::random_device rd;
	static std::mt19937 g(rd());

	std::vector<std::shared_ptr<Card>> deck;
	for (int i = 0; i < 3; ++i) {
		auto tmp = createNormalCards();
		deck.insert(deck.end(), tmp.begin(), tmp.end());
	}

	auto tmp = createNormalCards();
	deck.insert(deck.end(), tmp.begin(), tmp.end());

	tmp = createNormalCards();
	deck.insert(deck.end(), tmp.begin(), tmp.end());

	tmp = createSpecialCards();
	deck.insert(deck.end(), tmp.begin(), tmp.end());

	std::shuffle(deck.begin(), deck.end(), g);

	return deck;
}
