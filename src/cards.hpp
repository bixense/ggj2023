#pragma once

#include <memory>
#include <vector>

class Card;

std::vector<std::shared_ptr<Card>> createNewDeck();
