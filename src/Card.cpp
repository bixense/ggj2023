#include "Card.hpp"

#include "Options.hpp"

#include <utility>

std::shared_ptr<jngl::Font> Card::font = nullptr;
std::shared_ptr<jngl::Font> Card::fontSmall = nullptr;

Card::Card(std::string name, int kcal, int celcius, int salt, Special special, Attack attack)
: sprite("card"), picture(name), name(std::move(name)), kcal(kcal), celcius(celcius), salt(salt),
  headline(this->name), size(200, 325), special(special), attack(attack) {
	if (!font) {
		font = std::make_shared<jngl::Font>("Poppins-Bold.ttf", 18);
		fontSmall = std::make_shared<jngl::Font>("SourceHanSansCN-Heavy.otf", 16);
	}
	headline.setAlign(jngl::Alignment::CENTER);
	headline.setFont(*font);
	headline.setCenter(0, -140);
	kcalText.setFont(*fontSmall);
	if (kcal != 0) {
		kcalText.setText(std::to_string(kcal) + " kcal");
	}
	kcalText.setPos(-80, 0);
	celciusText.setFont(*fontSmall);
	if (celcius != 0) {
		celciusText.setText((celcius > 0 ? std::string("+") : std::string()) +
		                    std::to_string(celcius) + " °C");
	}
	celciusText.setPos(-80, 30);
	saltText.setFont(*fontSmall);
	if (salt != 0) {
		saltText.setText((salt > 0 ? std::string("+") : std::string()) + std::to_string(salt) +
		                 " mg Salz");
	}
	saltText.setPos(-80, 60);
	specialText.setFont(*fontSmall);
	specialText.setPos(-80, 100);
	switch (special) {
	case Special::NONE:
		break;
	case Special::RESET_SALT:
		specialText.setText("setzt Salz\nauf 0 mg");
		break;
	case Special::HALVE_SALT:
		specialText.setText("halbiert den\nSalzgehalt");
		break;
	case Special::RESET_HEAT:
		specialText.setText("stellt die Platte\nauf 20 °C");
		break;
	case Special::RESET_ALL:
		specialText.setText("versaut das\nEssen");
		break;
	case Special::HALVE_HEAT:
		specialText.setText("halbiert die\nTemperatur");
		break;
	case Special::DRAW_CARD:
		specialText.setText("ziehe eine\nKarte");
		break;
	case Special::FILL_HAND:
		specialText.setText("füllt Hand auf\n4 Karten auf");
		break;
	case Special::DOUBLE_KCAL:
		specialText.setText("verdoppelt die\nKalorien");
		break;
	case Special::POT_INCREASE:
		specialText.setText("großer\nTopf 5s");
		break;
	}
	attackText.setFont(*fontSmall);
	attackText.setPos(-80, 100);
	switch (attack) {
	case Attack::NONE:
		break;
	case Attack::SALT_OVERLOAD:
		attackText.setText("Gegner\n+40 mg Salz");
		break;
	case Attack::HEAT_OVERLOAD:
		attackText.setText("Gegner +90°C");
		break;
	case Attack::BLIND:
		attackText.setText("blendet Gegner\nfür 4 s");
		break;
	case Attack::BLIND_SELF:
		attackText.setText("blendet dich\nfür 4 s");
		break;
	case Attack::POT_SHRINK:
		attackText.setText("Gegner kleiner\nTopf 5s");
		break;
	case Attack::MIRROR:
		attackText.setText("spiegelt den\nGegner für 9 s");
		break;
	}
	setCenter({ 0, 500 });
	picture.setCenter(0, -70);
}

void Card::moveTo(const jngl::Vec2 center) {
	targetCenter = center;
}

jngl::Vec2 Card::getCenter() const {
	return center;
}

void Card::setCenter(const jngl::Vec2 center) {
	targetCenter = this->center = center;
}

void Card::step() {
	center += 0.15 * (targetCenter - center);
	scale += 0.2f * ((remove ? 0.f : (mouseOver ? 1.2f : 1.0f)) - scale);
	mouseOver = false;
}

void Card::draw() const {
	jngl::pushMatrix();
	jngl::translate(center.x, center.y);
	jngl::scale(scale);
	sprite.draw();
	picture.draw();
	jngl::setFontColor(0x111111_rgb);
	headline.draw();
	jngl::setFontColor(0x333333_rgb);
	kcalText.draw();
	celciusText.draw();
	saltText.draw();
	jngl::setFontColor(0x337eaa_rgb);
	specialText.draw();
	jngl::setFontColor(0xbb3333_rgb);
	attackText.draw();
	jngl::popMatrix();
}

bool Card::checkMouseOver(const jngl::Vec2 mousePos, bool sensitive) {
	const auto bottomLeft = center - size / 2;
	if (mousePos.x < bottomLeft.x || mousePos.y < bottomLeft.y) {
		return false;
	}
	const auto topRight = center + size / 2;
	if (mousePos.x > topRight.x || mousePos.y > topRight.y) {
		return false;
	}
	mouseOver = sensitive;
	return true;
}

bool Card::isMouseOver() const {
	return mouseOver;
}

bool Card::isAnimating() const {
	return boost::qvm::mag_sqr(targetCenter - center) > 100;
}

void Card::startRemove() {
	remove = true;
}

int Card::getKcal() const {
	return kcal;
}

int Card::getCelcius() const {
	return celcius;
}

int Card::getSalt() const {
	return salt;
}

Special Card::getSpecial() const {
	return special;
}

Attack Card::getAttack() const {
	return attack;
}
