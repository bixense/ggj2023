#pragma once

#include "Game.hpp"
#include "gui/Button.hpp"

class Countdown : public jngl::Work {
public:
	explicit Countdown();
	~Countdown() override;

	void step() override;
	void draw() const override;

	void onQuitEvent() override;

private:
	std::shared_ptr<MouseTracker> mouse1{ std::make_shared<MouseTracker>(1) };
	std::shared_ptr<MouseTracker> mouse2{ std::make_shared<MouseTracker>(2) };
	std::shared_ptr<Game> game;
	jngl::FrameBuffer fbo;
	jngl::FrameBuffer fbo2;

	jngl::Container container;

	jngl::Shader fragment;
	jngl::ShaderProgram blur;

	jngl::Widget* ready1;
	jngl::Widget* ready2;
	int framesPassed = -1;

	constexpr static double REDUCE_RESOLUTION = 4;

};
