#version 100

uniform sampler2D tex;
uniform int width;
uniform int height;

varying mediump vec2 texCoord;

// 0 - no blur
// 1 - 3x3 blur
// 2 - 5x5 blur
// 3 - 7x7 blur
// ...
const int size = 10;

void main() {
	mediump float stepSizeX = 1.0 / FBO_WIDTH;  // This should be 1/width
	mediump float stepSizeY = 1.0 / FBO_HEIGHT; // and 1/height
	lowp vec4 sum = vec4(0.0);
	int numOfSamples = 0;
	for (int x = -size; x <= size; x++) {
		for (int y = -size; y <= size; y++) {
			if (x*x + y*y < size*size) {
				sum += texture2D(tex, vec2(texCoord.x + float(x) * stepSizeX,
				                         texCoord.y + float(y) * stepSizeY));
				++numOfSamples;
			}
		}
	}
	gl_FragColor = vec4(sum.rgb / float(numOfSamples), 1);
}
